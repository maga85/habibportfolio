import os
from django.db.models import Q, Count
from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group, User
from .forms import PostForm, ContactFormu
from .models import *
from account.models import Subscribe
from django.utils.text import slugify

# Create your views here.

def get_category_count():
    queryset = Post \
        .objects \
        .values('categories__title') \
        .annotate(Count('categories__title'))
    return queryset

def get_tag_count():
    querysetTag = Post \
        .objects \
        .values('tags__title') \
        .annotate(Count('tags__title'))
    return querysetTag


def search(request):
    setting = Setting.objects.get(pk=1)
    queryset = Post.objects.all()
    query = request.GET.get('q')
    if query:
        queryset = queryset.filter(
            Q(title__icontains=query) |
            Q(overview__icontains=query) 
        ).distinct()

    context = {
        'queryset': queryset
    }
    return render(request, 'search-results.html', context)



def home(request):
    setting = Setting.objects.get(pk=1)
    testmonials = Testimonial.objects.all()
    services = Service.objects.all()
    about = About.objects.get(pk=1)
    recent_portfolio = Portfolio.objects.all().order_by('-id')[:3]

    
    context= {
        'testmonials':testmonials,
        'services':services,
        'about':about,
        'recent_portfolio':recent_portfolio,
        'setting':setting,
    }
    return render(request, 'index.html', context)

def about(request):
    setting = Setting.objects.get(pk=1)
    testmonials = Testimonial.objects.all()
    about = About.objects.get(pk=1)
    context= {
        'testmonials':testmonials,
        'about':about,
        'setting':setting,
    }
    return render(request, 'about.html', context)

def services(request):
    testmonials = Testimonial.objects.all()
    services = Service.objects.all()
    setting = Setting.objects.get(pk=1)
    context= {
        'testmonials':testmonials,
        'services':services,
        'setting':setting,
    }
    return render(request, 'services.html', context)

def portfolio(request):
    testmonials = Testimonial.objects.all()
    portfolios = Portfolio.objects.all()
    setting = Setting.objects.get(pk=1)


    paginator = Paginator(portfolios, 6)
    page_request_var = 'page'
    page = request.GET.get(page_request_var)
    try:
        paginated_queryset = paginator.page(page)
    except PageNotAnInteger:
        paginated_queryset = paginator.page(1) 
    except EmptyPage:
        paginated_queryset = paginator.page(paginator.num_pages)  
    context= {
        'testmonials':testmonials,
        'queryset': paginated_queryset,
        'page_request_var': page_request_var,
        'setting':setting,
    }
    return render(request, 'portfolio.html', context)


def contactMessage(request):
    if request.method == 'POST':             #form post edildiyse
        form = ContactFormu(request.POST)
        if form.is_valid():
            data = ContactFormMessage()            # model ile baglanti kur
            data.name = form.cleaned_data['name']  #formdan bilgiyi al
            data.email = form.cleaned_data['email']
            data.subject = form.cleaned_data['subject']
            data.message = form.cleaned_data['message']
            data.ip = request.META.get('REMOTE_ADDR') #client computer ip address
            data.save()   #veritabanina kaydet
            messages.success(request, "Your Message has been sent succesfully. Thank you")
            return redirect('contact')


    form = ContactFormu()
    setting = Setting.objects.get(pk=1)
    context= {
        'setting':setting,
        'form':form,
    }
    return render(request, 'contact.html', context)

def blog(request):
    category_count = get_category_count()
    tag_count = get_tag_count()
    #print(tag_count)
    setting = Setting.objects.get(pk=1)
    most_recent = Post.objects.order_by('-created_date')[:4]
    post_list = Post.objects.all()
    paginator = Paginator(post_list, 2)
    page_request_var = 'page'
    page = request.GET.get(page_request_var)
    try:
        paginated_queryset = paginator.page(page)
    except PageNotAnInteger:
        paginated_queryset = paginator.page(1) 
    except EmptyPage:
        paginated_queryset = paginator.page(paginator.num_pages)  


    if request.method =="POST":
        email = request.POST["email"]
        new_signup = Subscribe()
        new_signup.email = email
        new_signup.save()
        messages.success(request, "You subscribed. Thank you")


    context = {
        'queryset': paginated_queryset,
        'most_recent': most_recent,
        'page_request_var': page_request_var,
        'category_count': category_count,
        'tag_count': tag_count,
        'setting':setting,
    }
    return render(request, 'blog.html',context)

def blogDetail(request, slug):
    post = get_object_or_404(Post, slug=slug)
    category_count = get_category_count()
    tag_count = get_tag_count()
    most_recent = Post.objects.order_by('-created_date')[:4]
    setting = Setting.objects.get(pk=1)
    # PostView.objects.get_or_create(user=request.user, post=post)
    post.view_count = post.view_count + 1
    post.save()

    #subscribe
    if request.method =="POST":
        email = request.POST["email"]
        new_signup = Subscribe()
        new_signup.email = email
        new_signup.save()
        messages.success(request, "You subscribed. Thank you")

    

    context = {
        'post': post,
        'most_recent': most_recent,
        'category_count': category_count,
        'tag_count': tag_count,
        'setting':setting,
    }
    return render(request, 'blog-detail.html', context)

def blogCreate(request):
    setting = Setting.objects.get(pk=1)
    title='Create'
    form = PostForm(request.POST or None, request.FILES or None)
    author = request.user
    if request.method == "POST":
        if form.is_valid():
            post = form.save(commit=False)
            post.slug = slugify(post.title)
            form.instance.author = author
            form.save()
            return redirect(reverse("blog-detail", kwargs={
                'slug':form.instance.slug
            }))
    context = {
        'title':title,
        'form':form,
        'setting':setting,
    }
    return render(request, 'blog-create.html', context)



def blog_update(request, slug):
    setting = Setting.objects.get(pk=1)
    title='Update'
    post = get_object_or_404(Post, slug=slug)
    form = PostForm(
        request.POST or None, 
        request.FILES or None, 
        instance=post)
    author = request.user
    if request.method == "POST":
        if form.is_valid():
            post = form.save(commit=False)
            post.slug = slugify(post.title)
            form.instance.author = author
            form.save()
            return redirect(reverse("blog-detail", kwargs={
                'slug':form.instance.slug
            }))
    context = {
        'title': title,
        'form':form,
        'setting':setting,
    }
    return render(request, 'blog-create.html', context)

def blog_delete(request, slug):
    post = get_object_or_404(Post, slug=slug)
    post.delete()
    return redirect(reverse("blog"))



def download(request, path):
    file_path = os.path.join(settings.MEDIA_ROOT, path)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/pdf")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http40

