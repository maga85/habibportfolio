# Generated by Django 3.1.7 on 2021-03-02 15:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0014_auto_20210302_1845'),
    ]

    operations = [
        migrations.AddField(
            model_name='setting',
            name='name',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='setting',
            name='specialty',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='setting',
            name='surname',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
