# Generated by Django 3.1.7 on 2021-03-02 11:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0003_service'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='image',
            field=models.FileField(blank=True, default='images/default.png', upload_to='images/'),
        ),
    ]
