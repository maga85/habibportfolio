# Generated by Django 3.1.7 on 2021-03-03 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0024_auto_20210303_1651'),
    ]

    operations = [
        migrations.AlterField(
            model_name='setting',
            name='phone',
            field=models.BigIntegerField(blank=True),
        ),
    ]
