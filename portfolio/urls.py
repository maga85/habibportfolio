from django.urls import path

from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('about/', views.about, name="about"),
    path('services/', views.services, name="services"),
    path('portfolio/', views.portfolio, name="portfolio"),
    path('contact/', views.contactMessage, name="contact"),
    path('blog/', views.blog, name="blog"),
    path('search/', views.search, name="search"),
    path('blogDetail/<slug:slug>/', views.blogDetail, name="blog-detail"),
    path('blogCreate/', views.blogCreate, name="blog-create"),
    path('blog/<slug:slug>/update/', views.blog_update, name="blog-update"),
    path('blog/<slug:slug>/delete/', views.blog_delete, name="blog-delete"),
]